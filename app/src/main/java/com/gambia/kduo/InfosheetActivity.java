/**
 * InfosheetActivity.java
 * Implements the app's infosheet activity
 * The infosheet activity sets up infosheet screens for "About" and "How to"
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.gambia.kduo;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.gambia.kduo.helpers.TransistorKeys;


/**
 * InfosheetActivity class
 */
public final class InfosheetActivity extends AppCompatActivity {


    TextView versionTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // get activity title from intent
        Intent intent = this.getIntent();

        // set activity title
        if (intent.hasExtra(TransistorKeys.EXTRA_INFOSHEET_TITLE)) {
            this.setTitle(intent.getStringExtra(TransistorKeys.EXTRA_INFOSHEET_TITLE));
        }

        // set activity view
        if (intent.hasExtra(TransistorKeys.EXTRA_INFOSHEET_CONTENT) && intent.getIntExtra(TransistorKeys.EXTRA_INFOSHEET_CONTENT, -1) == TransistorKeys.INFOSHEET_CONTENT_ABOUT) {
            setContentView(R.layout.fragment_infosheet_about);
        }




        PackageInfo pInfo = null;
        try {
            versionTxt = (TextView) findViewById(R.id.TV_app_version);
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            versionTxt.setText("Version " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }

}
